package App;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
//
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.miginfocom.swing.MigLayout;
import javax.swing.JEditorPane;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
//

public class MainFrame extends JFrame {
	
	private String City = "London";
	private String weatherHtmlUrl = 
			"http://api.openweathermap.org/data/2.5/weather?q=" + City + "&mode=html&apikey=ab3a3a4743c325386addd4cfe19adfba";
	private String weatherXmlUrl = 
			"http://api.openweathermap.org/data/2.5/weather?q=" + City + " &mode=xml&apikey=ab3a3a4743c325386addd4cfe19adfba";
	private static final long serialVersionUID = 2835872241589818550L;
	private JTextPane textPane;
	
	private JPanel contentPane;
	
	private DbConnect conn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public String getHTML(String urlToRead) throws Exception {
		StringBuilder result = new StringBuilder();
		URL url = new URL(urlToRead);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		for (String line = reader.readLine(); line != null; line = reader.readLine()) {
			if( !line.toLowerCase().contains("<script>") && !line.toLowerCase().contains("</script>") && (line.toLowerCase().contains(">") || line.toLowerCase().contains("<")) ) {
				result.append(line);
			}
		}
		
		reader.close();
		return result.toString();
	}
	
	public void renderHtml(JEditorPane editorPane) {
		try {
			weatherHTML = getHTML(weatherHtmlUrl);
			editorPane.setText(weatherHTML);

			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			parser.parse(weatherXmlUrl, new MyHandler());
			textPane.setText(parsedXml);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	String weatherHTML = "empty";
	String parsedXml = "empty";
	String AdresPomiaru = "";
	String WynikPomiaru = "";
	
	private final JButton btnNewButton = new JButton("select");
	private JList list;
	private JButton btnInsert;
	private JTextPane textPane_1;
	private JTextField textField;
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 591, 657);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		conn = new DbConnect();
		contentPane.setLayout(new MigLayout("", "[grow][grow]", "[grow][grow][][][grow]"));

		JEditorPane editorPane = new JEditorPane();
		contentPane.add(editorPane, "cell 0 0,grow");
		editorPane.setContentType("text/html");
		
		list = new JList();
		contentPane.add(list, "cell 1 0,grow");
		textPane = new JTextPane();
		contentPane.add(textPane, "cell 0 1,grow");
		textField = new JTextField();
		contentPane.add(textField, "cell 0 3");
		
		try {
			weatherHTML = getHTML(weatherHtmlUrl);
			editorPane.setText(weatherHTML);

			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			parser.parse(weatherXmlUrl, new MyHandler());
			textPane.setText(parsedXml);
			
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					conn.ExecuteSql("Select * from Pomiar");
					textPane.setText(conn.ResToStrning());
					conn.DisplayResult();
				}
			});
			contentPane.add(btnNewButton, "cell 0 2");
			
			btnInsert = new JButton("insert");
			btnInsert.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					City = textField.getText();
					weatherHtmlUrl = 
							"http://api.openweathermap.org/data/2.5/weather?q=" + City + "&mode=html&apikey=ab3a3a4743c325386addd4cfe19adfba";
					weatherXmlUrl = 
							"http://api.openweathermap.org/data/2.5/weather?q=" + City + " &mode=xml&apikey=ab3a3a4743c325386addd4cfe19adfba";
					renderHtml(editorPane);
					
					System.out.println(WynikPomiaru);
					float pomiar = Float.valueOf(WynikPomiaru) ; 
					conn.InsertPomiar(AdresPomiaru, "T", "15.10.2017 13:00:00", pomiar - 273);
				}
			});
			contentPane.add(btnInsert, "flowx,cell 0 3");
			

			textField.setColumns(10);
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	class MyHandler extends DefaultHandler {
		Map<String, Boolean> elements = new HashMap<String, Boolean>();
		StringBuilder result = new StringBuilder();
		String Adres = "";
		String Wynik = "";
		
		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException {
			elements.put(qName, true);
			if (qName.equals("city")) {
				result.append("Miasto: " + attributes.getValue("name") + "\n");
				Adres = attributes.getValue("name");
				
			}
			if (qName.equals("temperature")) {
				result.append("Temperatura: " + attributes.getValue("value") + "\n");
				Wynik = attributes.getValue("value");
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			super.characters(ch, start, length);			
			if(elements.get("city")) {
				result.append("Pa�stwo: " + new String(ch, start, length) + "\n");
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			elements.put(qName, false);
		}
		
		@Override
		public void endDocument() throws SAXException {
			super.endDocument();
			parsedXml = result.toString();
			WynikPomiaru = Wynik;
			AdresPomiaru = Adres;
		}
	}
	
	

}
