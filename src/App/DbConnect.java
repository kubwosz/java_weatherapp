package App;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnect {
	
	
public ResultSet resultSet;
	


	public DbConnect() {
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			
			Connection connection = DriverManager.getConnection("jdbc:hsqldb:file:data/Pogoda2.db", "SA", "");
			
			Statement statement = connection.createStatement();
			
			resultSet = statement.executeQuery("Select * from Pomiar");
						
			
			statement.execute("SHUTDOWN");
			statement.close();
			connection.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void ExecuteSql(String sql) {
		
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			
			Connection connection = DriverManager.getConnection("jdbc:hsqldb:file:data/Pogoda2.db", "SA", "");
			
			Statement statement = connection.createStatement();
			
			resultSet = statement.executeQuery(sql);
						
			statement.execute("SHUTDOWN");
			statement.close();
			connection.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	public void InsertPomiar(String Adres, String Kod, String Data, float Wynik) {
		ExecuteSql("Select max(ID_POMIARU) from Pomiar");
		int id;
		try {
			id = resultSet.getInt("ID_POMIARU");
			++id;
			
		} catch (SQLException e) {
			id = 1;
		}
		
		ExecuteSql("Insert into Pomiar Values('" + id + "',(TO_DATE('" + Data + "', 'dd.mm.yyyy hh24:mi:ss')),'" + Adres +"', '" + Kod + "', '" + Wynik + "')");
	}
	
	
	public void ClearDatabase() {
		ExecuteSql("Delete * from Pomiar");
		resultSet = null;
	}
	
	
	public void DisplayResult() {
		try {
			while(resultSet.next()) {
				System.out.println(resultSet.getString("ADRES") + " - " + resultSet.getString("KOD_WIELKOSCI") + " - " + resultSet.getString("WYNIK")
				+ " - " + resultSet.getDate("DATA_POMIARU"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public String ResToStrning() {
		String wyn = "";
		try {
			while(resultSet.next()) {
				wyn += resultSet.getString("ADRES") + " - " + resultSet.getString("KOD_WIELKOSCI") + " - " + resultSet.getString("WYNIK")
				+ " - " + resultSet.getDate("DATA_POMIARU") + "\n";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return wyn;
	}

}
